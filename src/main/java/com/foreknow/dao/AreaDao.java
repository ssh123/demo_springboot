package com.foreknow.dao;

import com.foreknow.entity.Area;

import java.util.List;

/**
 * Created by imac on 2018/5/8.
 */
public interface AreaDao {
    /**
     *
     * @return
     */
    List<Area> queryArea();

    /**
     *
     * @param queryId
     * @return
     */
    Area queryAreaById(int queryId);

    /**
     *
     * @param area
     * @return
     */
    int insertArea(Area area);

    /**
     *
     * @param area
     * @return
     */
    int updateArea(Area area);

    /**
     *
     * @param areaId
     * @return
     */
    int deleteArea(int areaId);
}
