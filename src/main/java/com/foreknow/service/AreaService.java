package com.foreknow.service;

import com.foreknow.entity.Area;

import java.util.List;

/**
 * Created by imac on 2018/5/8.
 */
public interface AreaService {
    /**
     *
     * @return
     */
    List<Area> getAreaList();

    /**
     *
     * @param queryId
     * @return
     */
    Area getAreaById(int queryId);

    /**
     *
     * @param area
     * @return
     */
    boolean addArea(Area area);

    /**
     *
     * @param area
     * @return
     */
    boolean modifyArea(Area area);

    /**
     *
     * @param areaId
     * @return
     */
    boolean deleteArea(int areaId);
}
