package com.foreknow.service.impl;

import com.foreknow.dao.AreaDao;
import com.foreknow.entity.Area;
import com.foreknow.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by imac on 2018/5/8.
 */
@Service
public class AreaServiceImpl implements AreaService {
    @Autowired
    private AreaDao areaDao;
    @Override
    public List<Area> getAreaList() {
        return areaDao.queryArea();
    }

    @Override
    public Area getAreaById(int queryId) {
        //int a = 1/0;
        return areaDao.queryAreaById(queryId);
    }

    @Transactional
    @Override
    public boolean addArea(Area area) {
        if(area.getAreaName()!=null && !"".equals(area.getAreaName())){
            area.setCreateTime(new Date());
            area.setLastEditTime(new Date());
            try{
                int effectedNum = areaDao.insertArea(area);
                if(effectedNum>0){
                    return true;
                }else {
                    throw new RuntimeException("插入区域信息失败！");
                }
            }catch(Exception e) {
                throw new RuntimeException("插入区域信息失败！"+e.getMessage());
            }
        }else {
            throw new RuntimeException("信息不能为空！");
        }
    }

    @Transactional
    @Override
    public boolean modifyArea(Area area) {
        if(area.getAreaId()!=null && !"".equals(area.getAreaId())){
            //area.setCreateTime(new Date());
            area.setLastEditTime(new Date());
            try{
                int effectedNum = areaDao.updateArea(area);
                if(effectedNum>0){
                    return true;
                }else {
                    throw new RuntimeException("插入区域信息失败！");
                }
            }catch(Exception e) {
                throw new RuntimeException("插入区域信息失败！"+e.getMessage());
            }
        }else {
            throw new RuntimeException("信息不能为空！");
        }
    }
    @Transactional
    @Override
    public boolean deleteArea(int areaId) {
        if(areaId>0){
            int effectedNum = areaDao.deleteArea(areaId);
            try{
                if(effectedNum>0){
                    return true;
                }else {
                    throw new RuntimeException("插入区域信息失败！");
                }
            }catch(Exception e) {
                throw new RuntimeException("插入区域信息失败！"+e.getMessage());
            }
        }else {
            throw new RuntimeException("信息不能为空！");
        }
    }
}
